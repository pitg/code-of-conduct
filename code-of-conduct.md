# Code of Conduct for the Public Interest Technology Group

## Goals of the CoC

1. PITG members should behave according to a clear code of conduct;
2. Members’ comportment extends across professional communities;
3. Explicitly address the issue of attribution versus privacy issue.

## Introduction

The Public Interest Technology Group (PITG) is a loosely organized group of individuals who self-identify as public-interest technologists, or are otherwise working on and interested in, issues at the intersection of technology, rights, equity and advocacy.

We were formed during the 2016 IETF 96 meeting in Berlin. Our group created a home for already existing conversations and collaborations between public interest technologists, funders, IETF participants and others involved in Internet infrastructure governance and standardization.

Over the past four years, the group grew considerably reflecting the interest in, and need for, public interest advocacy in technical spaces. Our focus expanded beyond the IETF to include a number of other standardization organizations, including the World Wide Web Consortium (W3C) and the Institute of Electrical and Electronics Engineers (IEEE).

This led to the need for a clear Code of Conduct to ground collaboration between PITG participants within a shared set of values. This document sets out a Code of Conduct for PITG with general conduct practices for its participants, by documenting scope, principles, and the redressal policy for complaints. It is a living document that evolves to meet the needs of our growing group.

This document brings together best practices from the open tech community and (digital) rights organizations, the two communities from which most PITG participants draw. Events at which PITG participants gather may have their own specific code of conduct, which may add additional descriptions of expected/unacceptable conduct. Please note that this policy does not aim to cover instances of general disagreement between PITG participants.

## Scope

This code applies in all PITG meetings, events, and gatherings, and fora including those online and offline, whether or not coinciding with IETF and IRTF meetings, that are organized or hosted by PITG. Those who participate in PITG, whether online or offline, are expected to behave according to professional standards and demonstrate a commitment to the principles outlined in this code of conduct.

We call upon PITG members to consider this code of conduct as guiding for their behavior in the broader tech ecosystem, especially when representing or referencing the group’s work.

## Principles

PITG is committed to creating a safe space for its participants, one in which people of many different backgrounds are treated with dignity, decency, and respect. It also aims to provide a place where public interest technologists and others can discuss their work and concerns with a reasonable expectation of privacy. 

PITG functions as a space for public-interest technologists and others working on issues of rights, equity and tech to convene. To ensure these individuals can collaborate effectively, PITG invitation works through a web of trust model. Currently, the group’s discussion happens under the Chatham House rule, to maintain an appropriate balance between privacy and attribution. This means that the content of its discussions may be shared with others if the individuals remain unattributed.

We strive to build a community based on those principles for everyone, free of intimidation, discrimination, or hostility—regardless of gender identity and expression, sexual orientation, nationality, origin, race, ethnicity, religion, age, disability, or physical appearance. We do not tolerate harassment, based on the above-mentioned attributes or other unwelcome hostile or intimidating behavior. Examples of harassment include the use of offensive language or sexual imagery in public presentations and displays, degrading verbal comments, deliberate intimidation, stalking, harassing photography or recording, inappropriate and non-consensual physical contact, and unwelcome sexual attention.

## Calling on the Code

The aim of the redressal policy is for PITG to attain a resolution to the complaint while ensuring safety, dignity and respect for the complainant; and proportionality and due process for the respondent.

If you believe you have been harassed, notice that someone else is being harassed, or have any other concerns regarding violations of the Code of Conduct, in or outside of the PITG context, please raise your concern in confidence with a member of the Code of Conduct Team. If the person who is harassing you is on the Conduct Team, they will recuse themselves from handling your incident and a substitute member of the Conduct team will step in. Current the following individuals are on the Conduct Team:

- Mallory Knodel, mallory@mayfirst.org
- Shivan Sahib, shivankaulsahib@gmail.com
- Corinne Cath, corinnecath@gmail.com
- Gurshabad Grover, gurshabad@cis-india.org (Code of Conduct substitute member)

When a member of the Conduct Team receives a complaint from you, the following steps will be taken:

- One or more members of the Conduct Team will discuss the issue with you in a safe, quiet and private space, this includes a discussion of steps that remedy the violation.
- Take notes, with your consent, of what you say.
- One or more members of the Conduct Team will separately speak with the person(s) against whom the complaint is lodged.
- Decide appropriate remedies, and ask the respondent to comply with the said steps

The process will involve obtaining resolution while ensuring safety, dignity and respect for everyone involved. Participants asked to stop any conduct in violation of the code of conduct must comply immediately. The Conduct Team may take various actions and sanctions deemed appropriate when someone is found to have violated this policy, up to and including expulsion from PITG.

Please note that the Conduct Team reserves the rights to reject reports made in bad faith. Reports intended to silence legitimate criticism may be deleted without response.
